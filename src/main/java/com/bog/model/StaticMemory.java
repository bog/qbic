package com.bog.model;

public class StaticMemory implements Memory
{
    private int[] memory;
    private int pointer;
    
    public StaticMemory(int maxsize)
    {
	this.memory = new int[maxsize];

	for(int i=0; i<this.memory.length; i++)
	    {
		this.memory[i] = 0;
	    }

	this.pointer = 0;
    }
    
    @Override
    public void left()
    {
	this.pointer--;
    }

    @Override
    public void right()
    {
	this.pointer++;
    }

    @Override
    public int pos()
    {
	return this.pointer;
    }
    
    @Override
    public void inc(int val)
    {
	this.memory[this.pointer] += val;
    }

    @Override
    public void dec(int val)
    {
	this.memory[this.pointer] -= val;
    }

    @Override
    public void set(int val)
    {
	this.memory[this.pointer] = val;
    }
    
    @Override
    public int get()
    {
	return this.memory[this.pointer];
    }
}
