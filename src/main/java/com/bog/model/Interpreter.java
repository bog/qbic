package com.bog.model;

import com.bog.model.ast.*;
import java.io.Reader;
import java.io.Writer;

public class Interpreter implements BrainfuckVisitor
{
    protected Memory memory;
    protected Reader reader;
    protected Writer writer;
    
    public Interpreter(Reader reader, Writer writer)
    {
	this.reader = reader;
	this.writer = writer;
	this.memory = new StaticMemory(256);
    }

    @Override
    public Object visit(SimpleNode node, Object data)
    {
	return data;
    }

    @Override
    public Object visit(ASTStart node, Object data)
    {
	return node.childrenAccept(this, data);
    }

    @Override
    public Object visit(ASTPlus node, Object data)
    {
	this.memory.inc(1);
	return node.childrenAccept(this, data);
    }

    @Override
    public Object visit(ASTMinus node, Object data)
    {
	this.memory.dec(1);
	return node.childrenAccept(this, data);
    }
    
    @Override
    public Object visit(ASTLeft node, Object data)
    {
	this.memory.left();
	return node.childrenAccept(this, data);
    }
    
    @Override
    public Object visit(ASTRight node, Object data)
    {
	this.memory.right();
	return node.childrenAccept(this, data);
    }
    
    @Override
    public Object visit(ASTLoop node, Object data)
    {
	if( this.memory.get() > 0 )
	    {
		node.childrenAccept(this, data);
		return node.jjtAccept( this, data );
	    }
	
	return data;
    }
    
    @Override
    public Object visit(ASTDot node, Object data)
    {
	try
	    {
		this.writer.write( this.memory.get() );
		this.writer.flush();
	    }
	catch(Exception e)
	    {
		e.printStackTrace();
	    }
	
	return node.childrenAccept(this, data);
    }
    
    @Override
    public Object visit(ASTComma node, Object data)
    {
	try
	    {
		int n = this.reader.read();
		this.memory.set(n);
	    }
	catch(Exception e)
	    {
		e.printStackTrace();
	    }
	
	return node.childrenAccept(this, data);
    }
}
