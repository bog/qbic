package com.bog.model;

public abstract interface Memory
{
    void left();
    void right();
    int pos();
    void inc(int val);
    void dec(int val);
    void set(int val);
    int get();
}
