package com.bog.model;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.io.BufferedReader;

import java.io.FileOutputStream;
import java.io.OutputStreamWriter;
import java.io.BufferedWriter;

public class EditorBuffer
{
    public static void save(File file, String str)
	throws Exception
    {
	FileOutputStream fos = new FileOutputStream(file);
	OutputStreamWriter output_writer = new OutputStreamWriter(fos);
	BufferedWriter writer = new BufferedWriter(output_writer);

	writer.write(str);
	writer.flush();
    }

    public static String load(File file)
	throws Exception
    {
	String str = "";
	FileInputStream input = new FileInputStream(file);
	InputStreamReader input_reader = new InputStreamReader(input);
	BufferedReader reader = new BufferedReader(input_reader);
	String line = "";

	while(line != null)
	    {
		line = reader.readLine();
		if(line != null)
		    {
			str += line + "\n";
		    }
	    }

	return str;
    }
}
