package com.bog.view;

import java.io.StringWriter;
import com.bog.model.Interpreter;
import com.bog.model.ast.*;
// javafx
import javafx.scene.control.TextInputDialog;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;

public class VisualInterpreter extends Interpreter
{
    private Editor editor;
    
    public VisualInterpreter(Editor editor)
    {
	super(null, new StringWriter());
	this.editor = editor;
    }

    @Override
    public Object visit(ASTStart node, Object data)
    {
	Object next_data = super.visit(node, data);
	
	Alert alert = new Alert(Alert.AlertType.INFORMATION
				, this.writer.toString()
				, ButtonType.CLOSE);
	alert.setTitle("Execution result");
	alert.showAndWait();
	
	return next_data;
    }
    
    @Override
    public Object visit(ASTComma node, Object data)
    {
	try
	    {
		TextInputDialog input = new TextInputDialog();
		input.showAndWait();
		
		String result = input.getResult();
		int n = 0;
		
		if(result != null)
		    {
			char[] c = {result.charAt(0)};
			n = Character.codePointAt(c, 0);
		    }
		
		this.memory.set(n);
	    }
	catch(Exception e)
	    {
		this.editor.showException(e);
	    }
	
	return node.childrenAccept(this, data);
    }
}
