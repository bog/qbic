package com.bog.view;

import javafx.scene.control.Menu;
import javafx.scene.control.MenuItem;
import javafx.scene.control.MenuBar;
import javafx.scene.layout.BorderPane;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.input.KeyCombination;
import javafx.scene.input.KeyCharacterCombination;
import javafx.scene.input.KeyCodeCombination;
import javafx.scene.input.KeyCode;


public class Menus
{
    private BorderPane border;

    private MenuBar menu_bar;

    private Menu file_menu;
    private MenuItem new_file;
    private MenuItem save;
    private MenuItem open;
    private MenuItem quit;

    private Menu brainfuck_menu;
    private MenuItem execute;
    
    public Menus(BorderPane border)
    {
	this.border = border;

	this.menu_bar = new MenuBar();
	this.border.setTop(this.menu_bar);

	// File Menu
	{
	    this.file_menu = new Menu("File");
	    this.new_file = new MenuItem("New");
	    this.save = new MenuItem("Save");
	    this.save.setDisable(true);
	    this.open = new MenuItem("Open");
	    this.quit = new MenuItem("Quit");
	    
	    this.file_menu.getItems().addAll(this.new_file
					     , this.save
					     , this.open
					     , this.quit);
	}

	// brainfuck menu
	{
	    this.brainfuck_menu = new Menu("Brainfuck");
	    this.execute = new MenuItem("Execute");
	    this.brainfuck_menu.getItems().addAll(this.execute);
	}
	
	this.menu_bar.getMenus().addAll(this.file_menu
					, this.brainfuck_menu);

	disableTabRelated(true);

	initKeys();
    }

    private void initKeys()
    {
	KeyCombination.Modifier ctrl;
	ctrl = KeyCombination.CONTROL_DOWN;

	KeyCharacterCombination k0;
	k0 = new KeyCharacterCombination ("N", ctrl);
	this.new_file.setAccelerator(k0);

	KeyCharacterCombination k1;
	k1 = new KeyCharacterCombination ("S", ctrl);
	this.save.setAccelerator(k1);

	KeyCharacterCombination k2;
	k2 = new KeyCharacterCombination ("O", ctrl);
	this.open.setAccelerator(k2);

	KeyCharacterCombination k3;
	k3 = new KeyCharacterCombination ("Q", ctrl);
	this.quit.setAccelerator(k3);

	KeyCodeCombination k4;
	k4 = new KeyCodeCombination(KeyCode.F5);
	this.execute.setAccelerator(k4);
    }
    
    public void setNewFileAction(EventHandler<ActionEvent> e)
    {
	this.new_file.setOnAction(e);
    }

    public void setSaveAction(EventHandler<ActionEvent> e)
    {
	this.save.setOnAction(e);
    }

    public void setOpenAction(EventHandler<ActionEvent> e)
    {
	this.open.setOnAction(e);
    }
    
    public void setQuitAction(EventHandler<ActionEvent> e)
    {
	this.quit.setOnAction(e);
    }

    public void setExecuteAction(EventHandler<ActionEvent> e)
    {
	this.execute.setOnAction(e);
    }

    public void disableTabRelated(boolean disable)
    {
	this.save.setDisable(disable);
	this.execute.setDisable(disable);
    }

    public void disableSave(boolean disable)
    {
	this.save.setDisable(disable);
    }
}
