package com.bog.view;

//java
import java.util.List;
import java.io.File;
import java.io.ByteArrayInputStream;
import java.io.StringWriter;
import java.io.StringReader;
import java.util.HashMap;
// model
import com.bog.model.EditorBuffer;
import com.bog.model.ast.Brainfuck;
import com.bog.model.ast.SimpleNode;
import com.bog.model.ast.ParseException;
// javafx
import javafx.application.Application;
import javafx.stage.Stage;
import javafx.stage.FileChooser;
import javafx.stage.FileChooser.ExtensionFilter;
import javafx.scene.Scene;
import javafx.scene.layout.BorderPane;
import javafx.scene.control.TabPane;
import javafx.scene.control.Tab;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.Alert;
import javafx.event.ActionEvent;
//richtextfx
import org.fxmisc.richtext.CodeArea;

public class Editor extends Application
{
    public static void main(String[] args)
    {
	launch(args);
    }

    // javafx
    private Stage stage;
    private Menus menus;
    private BorderPane border;
    private TabPane tabs;
    private Tab current_tab;
    private Label result_label;
    private FileChooser file_chooser;
    private HashMap<Tab, File> files_map;

    @Override
    public void start(Stage stage)
    {
	this.files_map = new HashMap<Tab, File>();

	initScene();
	initActions();
	this.stage = stage;
	this.stage.setScene( new Scene(this.border) );
	this.stage.setTitle("QBIC : Quiescent Brainfuck" +
			    " Interpreter and Compiler");
	this.stage.setWidth(640);
	this.stage.setHeight(480);
	this.stage.show();
    }

    private void initScene()
    {
	initFileChooser();
	initBorder();
    }

    private void initBorder()
    {
	this.border = new BorderPane();

	// top
	this.menus = new Menus(this.border);

	// center
	this.tabs = new TabPane();
	this.tabs.getSelectionModel().selectedItemProperty().addListener((o, t1, t2)->{
		onTabSelectionChanged(t1, t2);
	    });
	this.border.setCenter(this.tabs);

	// bottom
	this.result_label = new Label();
	this.border.setBottom(this.result_label);
    }

    private void initFileChooser()
    {
	this.file_chooser = new FileChooser();
	this.file_chooser.setTitle("Brainfuck file");

	ExtensionFilter ef1 = new ExtensionFilter("Brainfuck", "*.bf");
	ExtensionFilter ef2 = new ExtensionFilter("All", "*");

	this.file_chooser.getExtensionFilters().addAll(ef1, ef2);
    }

    private void initActions()
    {
	this.menus.setNewFileAction(event->{
		File f = this.file_chooser.showSaveDialog(this.stage);
		try
		    {
			f.createNewFile();
			openCodeTab(f);
		    }

		catch(Exception e)
		    {
			showException(e);
		    }
	    });
	
	this.menus.setOpenAction(event->{
		List<File> fs;
		fs = this.file_chooser.showOpenMultipleDialog(this.stage);
		for(File f : fs)
		    {
			openCodeTab(f);
		    }
	    });

	this.menus.setSaveAction(event->{
		saveCurrentTab();
	    });
	
	this.menus.setQuitAction(event->{
		System.exit(0);
	    });

	this.menus.setExecuteAction(event->{
		executeCurrentTab();
	    });
    }

    private void openCodeTab(File file)
    {
	try
	    {
		Tab tab = new Tab( file.getName() );
		CodeArea area = new CodeArea();

		tab.setContent(area);
		area.replaceText( 0, 1, EditorBuffer.load(file) );

		area.textProperty().addListener((o, newv, oldv)->{
			this.menus.disableSave(false);
			updateTabText(tab, true);
		    });
		
		this.tabs.getSelectionModel().select(tab);
		this.tabs.getTabs().add(tab);
		this.files_map.put(tab, file);
	    }
	catch(Exception e)
	    {
		showException(e);
	    }
    }

    private void onTabSelectionChanged(Tab old_tab, Tab new_tab)
    {
	this.current_tab = new_tab;

	if(this.current_tab == null)
	    {
		this.menus.disableTabRelated(true);
	    }
	else
	    {
		this.menus.disableTabRelated(false);
	    }
    }

    private void saveCurrentTab()
    {
	if(this.current_tab == null)
	    {
		printError("Cannot save.");
		return;
	    }

	File file = this.files_map.get(this.current_tab);

	if(file == null)
	    {
		printError( "Cannot find "
			   + file.getAbsolutePath() );
		return;
	    }

	CodeArea code_area = (CodeArea) this.current_tab.getContent();
	String code = code_area.getText();
	
	try
	    {
		file.delete();
		file.createNewFile();
		EditorBuffer.save(file, code);
		this.menus.disableSave(true);
		updateTabText(this.current_tab, false);
	    }
	catch(Exception e)
	    {
		printError("Cannot save.");
		showException(e);
	    }
    }

    private void executeCurrentTab()
    {
	if(this.current_tab == null)
	    {
		printError("Cannot execute.");
		return;
	    }

	CodeArea code_area = (CodeArea)
	    (this.current_tab.getContent());

	String code = code_area.getText();

	try
	    {
		ByteArrayInputStream input;
		input = new ByteArrayInputStream(code.getBytes());

		Brainfuck brainfuck;
		brainfuck = new Brainfuck(input);

		SimpleNode root = brainfuck.start();

		VisualInterpreter interpreter;
		interpreter = new VisualInterpreter(this);

		root.jjtAccept(interpreter, null);
	    }
	catch(Exception e)
	    {
		showException(e);
	    }
    }

    private void updateTabText(Tab tab, boolean modified)
    {
	String name = this.files_map.get(tab).getName();
	
	if(modified)
	    {
		tab.setText("*" + name);
	    }
	else
	    {
		tab.setText(name);
	    }
    }
    
    private void printExec(String msg)
    {
	this.result_label.setText("EXEC : "
				  + msg);
    }

    private void printError(String msg)
    {
	this.result_label.setText("ERROR : "
				  + msg);
    }

    public void showException(Exception e)
    {
	Alert alert = new Alert( Alert.AlertType.ERROR
				 , e.toString() );
	alert.showAndWait();	
    }
}
