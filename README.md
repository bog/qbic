QBIC version 0.0

# Welcome

Hello :)
You are in the main developement page of QBIC.
QBIC stand for _Quiscent Brainfuck Interpreter and Compiler_.
Please, take a sit, prepare some coffee and write brainfuck.

## Functionnalities
The current version of QBIC allows you to :
- create a new source file,
- edit source files using multiples tabs,
- open an existing source files,
- save a sources files,
- interpret a source files

More functionnalities will come in further versions !

## How to build QBIC

`gradle build`

## How to launch the IDE

Execute the jar.